<?php
require __DIR__ . '/App/autoload.php';


use SebastianBergmann\Timer\ResourceUsageFormatter;
use SebastianBergmann\Timer\Timer;

$ctrl = $_GET['ctrl'] ?? 'Index';
$action = $_GET['act'] ?? 'All';
$class = '\App\Controllers\\' . $ctrl;

try {
    if (!class_exists($class)) {
        throw new \Exception('Ошибка 404');
    }

    $reflect = new \ReflectionClass($class);
    $methods = $reflect->getMethods();
    if (!$reflect->hasMethod($action)) {
        throw new \Exception('Ошибка 404');


    }


    $ctrl = new $class;
    $ctrl->action($action);
} catch (Exception $e) {
    echo 'Ошибка:' . $e->getCode(), "\n", $e->getMessage(), "\n";

} catch (\App\DbException $e) {
    echo $e->getMessage(), "\n";
} catch (PDOException $e) {
    echo $e->getMessage();
} finally {
    echo '<hr> <a href="?ctrl=Article&act=triads">Главная страница</a>';
}

