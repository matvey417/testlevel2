<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="?ctrl=AdminPanel&access=admin&act=insert" method="post">
    Заголовок: <input type="text" size="200" name="header">
    <br>
    Описание: <input type="text" size="202" name="content">
    <br>
    Автор:
    <select name="idAuthor" size="5" multiple>
        <?php foreach ($this->authors as $author) :
            echo '<option value="' . $author->id . '">' . $author->name . '</option>';
        endforeach; ?>

    </select>
    <input type="hidden" name="insert" value="true">
    <button type="submit">Добавить новость</button>

    <a href="?ctrl=AdminPanel&access=admin&act=admPanel">Назад</a>
</form>
</body>
</html>