
<body>
<form action="?ctrl=AdminPanel&access=admin&act=update" method="post">
    Заголовок: <input type="text" size="200" name="header" value="<?php echo $this->article->header ?>">
    <br>
    Описание: <input type="text" size="200" name="content" value="<?php echo $this->article->content ?>">
    <input type="hidden" name="update" value="true">
    <br>
    <p>
        <select size="5" multiple name="idAuthor">
            <option disabled>Автор</option>

            <?php /** @var \App\Models\Authors $author */

            foreach ($this->authors as $author) : ?>
                <?php if ($author->id !== $this->article->getAuthor()->id) {
                    echo '<option value="' . $author->id . '">' . $author->name . '</option>';;
                } elseif ($author->id == $this->article->getAuthor()->id) {
                    echo '<option selected value="' . $author->id . '">' . $author->name . '</option>';;}?>
                <?php
            endforeach; ?>
        </select></p>
    <button type="submit" name="id" value="<?php echo $this->article->id ?>">Изменить</button>

    <a href="?ctrl=AdminPanel&access=admin&act=admPanel">Назад</a>
</form>

</body>
</html>