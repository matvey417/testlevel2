<?php


namespace App;


class Connection
{
    private static $instance;

    private function __construct()
    {
        self::$instance = Db::getInstance();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }
}