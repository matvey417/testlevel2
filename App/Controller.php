<?php


namespace App;


use App\Controllers\Article;
use SebastianBergmann\Timer\ResourceUsageFormatter;
use SebastianBergmann\Timer\Timer;

abstract class Controller
{
    protected $view;
    protected $accessRights;

    public function __construct()
    {
        $this->view = new View();
    }

    public function access(): bool
    {
        if ($_GET['access'] === 'admin') {
            $accessRights = true;
        } else {
            $accessRights = false;
            throw new \Exception('Панель админа для Вас недоступна', 452);
        }
        return $accessRights;
    }

    public function action($action)
    {
        $this->$action();
    }

    public static function timer()
    {
        $timer = new Timer;
        $timer->start();

        foreach (\range(0, 100000) as $i) {

        }
        $time = (new ResourceUsageFormatter)->resourceUsage($timer->stop());
        return $time;
    }

}
