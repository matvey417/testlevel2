<?php


namespace App\Controllers;


use App\Controller;
use App\View;
use App\Models\Authors;

class Article extends Controller
{


    public function triads()
    {

        $this->view->article = \App\Models\Article::findSeveral();

        $this->view->display(__DIR__ . '/../../template/index.php');
    }

    public function oneEntry()
    {
        $id = $_GET['id'];
        $authorId = $this->view->article = \App\Models\Article::findById($id);
        $this->view->author = Authors::findById($authorId->author_id);

        $this->view->display(__DIR__ . '/../../template/article.php');

    }



}