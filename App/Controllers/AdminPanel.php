<?php


namespace App\Controllers;


use App\Controller;
use App\Models\Authors;

class AdminPanel extends Controller
{

    public function news()
    {

        echo '<pre>';
        $news = \App\Models\Article::findAll();
        $admDataTable = new \App\AdminDataTable(
            $news,
            [
                'id' => function ($article) {
                    return $article->id;
                },
                'header' => function ($article) {
                    return $article->header;
                },
                'content' => function ($article) {
                    return $article->content;
                }
            ]
        );

        $this->view->assign('article', $admDataTable->render());
        $this->view->display(__DIR__ . '/../../template/admData.php');

    }

    public function admPanel()
    {
        echo '<pre>';
        $this->access();

        $idDelete = $_POST['idDelete'];
        $article = new \App\Models\Article();
        $article->header = $_POST['header'];
        $article->content = $_POST['content'];
        $article->author_id = $_POST['idAuthor'];
        $article->id = $_POST['id'];
        if (null !== $_POST['idAuthor']) {
            $article->save();
        }

        if ($idDelete !== null) {
            $article->delete($idDelete);
        }

        $this->view->article = \App\Models\Article::findAll();

        if (true === $this->access()) {

            $this->view->display(__DIR__ . '/../../template/adminpanel.php');
        }

    }

    public function insert()
    {
        if (isset($_POST['insert'])) {

            header("Location: ?ctrl=AdminPanel&access=admin&act=admPanel");
        }

        echo '<pre>';
        $article = new \App\Models\Article();
        $article->header = $_POST['header'];
        $article->content = $_POST['content'];
        $article->author_id = $_POST['idAuthor'];
        //$article = new \App\Models\Article();
        $article->insert();

        if (true === $this->access()) {
            $this->view->assign('article', \App\Models\Article::findAll());
            $this->view->assign('authors', Authors::findAll());
            $this->view->display(__DIR__ . '/../../template/insert.php');
        }

    }

    public function update()
    {

        if (isset($_POST['update'])) {
            header("Location: ?ctrl=AdminPanel&access=admin&act=admPanel");
        }
        echo '<pre>';
        $article = new \App\Models\Article();
        $article->header = $_POST['header'];
        $article->content = $_POST['content'];
        $article->author_id = $_POST['idAuthor'];
        $article->id = $_POST['id'];
        if (null !== $_POST['idAuthor']) {
            $article->save();
        }

        if (true === $this->access()) {
            $this->view->assign('article', \App\Models\Article::findById($_POST['idArticle']));
            $this->view->assign('authors', Authors::findAll());
            $this->view->display(__DIR__ . '/../../template/update.php');

        }

    }

}