<?php


namespace App\Controllers;


use App\Controller;
use App\Models\Authors;

class Index extends Controller
{


    public function actionTriads()
    {

        $this->view->article = \App\Models\Article::findSeveral();

        $this->view->display(__DIR__ . '/../../template/index.php');
    }


    public function All()
    {

        $this->view->article = \App\Models\Article::findAllGenerator();
        $this->view->assign('timer', \App\Controller::timer());
        $this->view->display(__DIR__ . '/../../template/index.php');

    }
}