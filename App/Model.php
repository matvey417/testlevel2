<?php


namespace App;


abstract class Model
{
    public const TABLE = '';
    public const SORTING = '';
    public const LIMIT = '';

    public $id;

    /**
     * @return array который содержит все записи из таблицы, таблица разная в зависимости от того из какого класса вызываем метод
     */
    public static function findAll()
    {

        $db = Db::getInstance();

        $sql = 'SELECT * FROM ' . static::TABLE;
         $result = $db->query($sql, [], static::class);
        return $result;

    }

    public static function findAllGenerator()
    {

        $db = Db::getInstance();

        $sql = 'SELECT * FROM ' . static::TABLE;
        foreach ($db->queryEach($sql, [], static::class) as $value) {
            $result[] = $value;
        }
        return $result;

    }

    public static function findSeveral()
    {
        $db = Db::getInstance();

        $sql = 'SELECT * FROM ' . static::TABLE . static::SORTING . static::LIMIT;
        return $result = $db->query($sql, [], static::class);

    }

    public static function addImage($imageName)
    {
        $db = Db::getInstance();
        $sql = 'INSERT INTO images (path) VALUE (:path)';
        $sth = $db->dbh->prepare($sql);
        $sth->execute([':path' => $imageName]);

    }

    /**
     * Добавляет новую новость
     */
    public function insert()
    {
        $fields = get_object_vars($this);

        $field = [];
        $substitution = [];

        foreach ($fields as $key => $value) {
            if ('id' == $key) {
                continue;
            }
            $field[] = $key;
            $substitution[':' . $key] = $value;
        }
        $flightTables = implode(',', $field);
        $sql = 'INSERT INTO ' . static::TABLE . '(' . $flightTables . ') VALUE (' . implode(',', array_keys($substitution)) . ')';
        var_dump($sql);
        var_dump($substitution);
        $db = Db::getInstance();
        $sth = $db->execution($sql, $substitution);
        $this->id = $db->getLastInsertId();
    }

    /**
     * обновляет данные по записи в таблице, тестил только на новостях
     */
    public function update()
    {
        $fields = get_object_vars($this);
        $field = [];
        $update = [];
        var_dump($fields['id']);
        foreach ($fields as $key => $value) {
            if ('id' == $key) {
                continue;
            }
            $field[] = $key . ' = :' . $key;
            $update[':' . $key] = $value;
        }
        $implodeFields = implode(',', $field);
        $sql = 'UPDATE ' . static::TABLE . ' SET ' . $implodeFields . ' where id = ' . $fields['id'];
        var_dump($sql);
        $db = Db::getInstance();
        $db->execution($sql, $update);

    }

    /**
     * метод в зависимости от того передали ли мы id существующей записи либо обновляет запись либо заносит новую
     *
     * @see update
     * @see insert
     */
    public function save()
    {

        if (null == $this->id) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    /**
     * @param $idDelete айди удаляемой записи
     */
    public function delete($idDelete)
    {
        $db = Db::getInstance();
        $sql = 'DELETE FROM ' . static::TABLE . ' WHERE id = ' . $idDelete;
        var_dump($sql);
        $db->execution($sql);
    }

    /**
     * @param $id
     * @return static который содержит в себе объект(только одну запись по id) каждое поле
     * которого является полем в таблице, таблица в базе берется
     * в зависимости от того из какого класса вызывается функция
     */
    public static function findById($id)
    {
        $db = Db::getInstance();
        $sql = 'SELECT * FROM ' . static::TABLE . ' WHERE id = :id';
        $data = $db->query($sql, [':id' => $id], static::class);
        return $data ? $data['0'] : null;
    }
}