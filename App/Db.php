<?php


namespace App;

class Db
{

    public $dbh;

    protected static $instance;

    /**
     * Db constructor.
     *
     * @throws \App\DbException
     */
    protected function __construct()
    {
        $config = (include __DIR__ . '/config.php')['db'];
        try {

            $this->dbh = new \PDO(
                'mysql:host=' .
                $config['host'] . ';dbname=' .
                $config['dbname'],
                $config['user'],
                $config['password']

            );
        } catch (\PDOException $e) {
            throw new DbException('Ошибка подключения базы, проверьте конфиг подклчюения: ' . $e->getMessage());
        }


    }

    protected function __clone()
    {
    }

    protected function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * @param $sql
     * @param array $data
     * @param $class
     * @return array
     * @internal подготовка запроса, и возврат того что он выволнил, например SELECT
     */
    public function query($sql, $data = [], $class)
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        $result = $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        if (empty($result)) {
            throw new DbException('Не удалось выполнить SQL запрос, проверьте правильность запроса query');
        }

        return $result;

    }

    /**
     * @param $sql
     * @param array $data
     * @param $class
     * @return array
     * @throws DbException
     * Для урока, используем генераторы
     */
    public function queryEach($sql, $data = [], $class)
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        $sth->setFetchMode(\PDO::FETCH_CLASS, $class);
        while ($feth = $sth->fetch()) {
            yield $feth;
        }
    }

    public function queryArray($sql, $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        return $sth->fetchAll();
    }

    /**
     * @see query
     */
    public function execution($sql, $data = [])
    {
        $sth = $this->dbh->prepare($sql);
        $result = $sth->execute($data);
        if (empty($result)) {
            throw new DbException('Не удалось выполнить SQL запрос, проверьте правильность запроса execution');
        }
        return $result;


    }

    public function getLastInsertId()
    {
        return $this->dbh->lastInsertId();
    }


}