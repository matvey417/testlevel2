<?php


namespace App;


class AdminDataTable
{
    public $model;
    public $fun;

    public function __construct($model, $function)
    {
        $this->model = $model;
        $this->fun = $function;

    }

    public function render()
    {
        $res = [];

        foreach ($this->model as $key => $model) {
            foreach ($this->fun as $k => $function) {
                $res[$key][$k] = $function($model);
            }
        }
        return $res;
    }
}

