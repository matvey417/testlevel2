<?php


namespace App\Models;

use App\Db;
use App\Model;

/**
 * Class Authors
 * @package App\Models
 * @method static null|Authors findById(int $id)
 */
class Authors extends Model
{
    public $name;

    public const TABLE = 'authors';

}
