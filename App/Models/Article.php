<?php


namespace App\Models;

use App\Db;
use App\Model;


/**
 * Class Article
 * @package App\Models
 */
class Article extends Model
{
    public $header;
    public $content;
    public $author_id;

    /** @var null|Authors $author */
    static protected $author;

    public const TABLE = 'news';
    public const SORTING = ' ORDER BY id DESC ';
    public const LIMIT = ' LIMIT 3';

    public function getAuthor(): ?Authors
    {
        if (null === $this->author) {
            $this->author = Authors::findById($this->author_id);
        }

        return $this->author;
    }

}
