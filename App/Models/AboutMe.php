<?php


namespace App\Models;


use App\Model;

class AboutMe extends Model
{
    public $description;
    public const TABLE = 'about_me';
}